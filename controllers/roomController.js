const controller = {};
const { validationResult } = require('express-validator');

controller.show = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM room', (err, rooms) => {
            if (err) {
                res.status(500).json(err);
                return;
            }
            res.render('./roomView/roomList', {
                data: rooms,
                session: req.session
            });
        });
    });
};

controller.add = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM room', (err, rooms) => {
            res.render('./roomView/roomAdd', {
                data1: rooms,
                session: req.session
            });
        });
    });
};

controller.new = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        return res.redirect('/room/add');
    } else {
        req.session.success = true;
        req.session.topic = "เพิ่มข้อมูลสำเร็จ!";
        const data = req.body;
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO room SET ?', [data], (err, result) => {
                if (err) {
                    res.json(err);
                }
                res.redirect('/room/list');
            });
        });
    }
};

controller.edit = (req, res) => {
    const { id } = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM room WHERE id = ?', id, (err, room) => {
            if (err) {
                return res.status(500).json(err);
            }
            res.render('./roomView/roomedit', {
                data1: room,
                session: req.session
            });
        });
    });
};

controller.save = (req, res) => {
    const errors = validationResult(req);
    const idToEdit = req.params;
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        return res.redirect('/room/edit/' + idToEdit);
    } else {
        req.session.success = true;
        req.session.topic = "แก้ไขข้อมูลสำเร็จ!";
        const idToEdit = req.params.id;
        const updatedData = {
            roomname: req.body.roomname
        };
        req.getConnection((err, conn) => {
            conn.query('UPDATE room SET ? WHERE id = ?', [updatedData, idToEdit], (err, result) => {
                if (err) {
                    return res.status(500).json(err);
                }
                res.redirect('/room/list');
            });
        });
    }
};

controller.delete = (req, res) => {
    const data = req.body.data;
    res.render('./roomView/Confirm', {
        data: data,
        session: req.session
    });
};

controller.delete1 = (req, res) => {
    req.session.success = true;
    req.session.topic = "ลบข้อมูลสำเร็จ!";
    const idToDelete = req.params.id;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM room WHERE id = ?', [idToDelete], (err, result) => {
            if (err) {
                res.json(err);
            }
            res.redirect('/room/list');
        });
    });
};

module.exports = controller;
