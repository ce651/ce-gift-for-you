const controller ={};
const { validationResult } = require('express-validator');

controller.show=(req,res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT deliveryperson.id,p.craftcom as craftcom,p.donationplace as donationplace,p.donationday as donationday,p.travel as travel,n.name as name,n.phone as phone,n.lineid as lineid,n.gender as gender FROM deliveryperson join prepare as p on deliveryperson.prepare=p.id join person as n on deliveryperson.person=n.id',(err, deliveryperson) => {
            if (err) {
                res.json(err);
            }
            res.render('./deliverypersonView/deliverypersonList',{
                data:deliveryperson,session:req.session
            });
        });
    });
};        

controller.add=(req,res) => {
    const data = null;
    req.getConnection((err, conn) => {
        conn.query('SELECT id, donationplace, travel FROM prepare', (err, prepare) => {
            conn.query('SELECT id, name FROM person', (err, person) => {
                    res.render('./deliverypersonView/deliverypersonAdd',{
                    data1:prepare,data2:person,data3:data,session: req.session
                    });
                });
            });
        });
    };

controller.new = (req, res) => {
    const errors = validationResult(req);
        if(!errors.isEmpty()){
            req.session.errors=errors;
            req.session.success =false;
            return  res.redirect('/deliveryperson/add')
        }else{
            req.session.success=true;
            req.session.topic="เพิ่มข้อมูลสำเร็จ!";
                const data = req.body;
                const prepare = data.donationplace;
                const person = data.name;
                const values = [prepare,person];
                req.getConnection((err, conn) => {
                conn.query('INSERT INTO deliveryperson (prepare, person) VALUES (?, ?)', values, (err,deliveryperson) => {
                    return res.redirect('/deliveryperson/list')
            });
        });      
    }
};

controller.edit = (req, res) => {
    const {id} = req.params; 
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM prepare', (err, prepare) => {
                conn.query('SELECT * FROM person', (err, person) => {
                        conn.query('SELECT * FROM deliveryperson WHERE id = ?',[id], (err, deliveryperson) => {
                        res.render('./deliverypersonView/deliverypersonEdit',{
                        data1:prepare,data2:person,data3:deliveryperson,session:req.session
                        });
                    });
                });
            });
        });
    };
      
    controller.save = (req, res) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            return res.redirect('/deliveryperson/edit/' + req.params.id);
        } else {
            req.session.success = true;
            req.session.topic = "แก้ไขข้อมูลสำเร็จ!";
            const { id } = req.params;
            const data = req.body;
            const prepare = data.donationplace; 
            const person = data.name;
            req.getConnection((err, conn) => {
                conn.query('UPDATE deliveryperson SET prepare=?, person=? WHERE id = ?', [ prepare, person, id], (err) => {
                    if (err) {
                        return res.status(500).json(err);
                    }
    
                    res.redirect('/deliveryperson/list');
                });
            });
        }
    };
controller.delete=(req, res) => {
    const data = req.body.data;
    res.render('./deliverypersonView/Confirm',{
        data:data,session:req.session
    });
};

controller.delete11 =(req,res) => {
    req.session.success=true;
    req.session.topic="ลบข้อมูลสำเร็จ!";
    const idToDelete = req.params.id;
    req.getConnection((err,conn) =>{
        conn.query('DELETE FROM deliveryperson  WHERE id = ?', [idToDelete], (err,deliveryperson ) => {
            if (err) {
                res.json(err);
            }
            res.redirect('/deliveryperson/list');
            });
        });
    };


module.exports=controller;