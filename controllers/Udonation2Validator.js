const { check } = require('express-validator');

exports.Udonation2 = [
    check('donation', "ชื่อผู้บริจาคไม่ถูกต้อง!").not().isEmpty().trim(),
    check('donationlist', "ประเภทอุปกรณ์ไม่ถูกต้อง!").isInt(),
];

exports.save = [
    check('donation', "ชื่อผู้บริจาคไม่ถูกต้อง!").not().isEmpty().trim(),
    check('donationlist', "ประเภทอุปกรณ์ไม่ถูกต้อง!").isInt(),
];
