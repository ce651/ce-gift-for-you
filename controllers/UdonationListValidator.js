const { check } = require('express-validator');

exports.UdonationList = [
    check('donationperson', "ผู้บริจาคไม่ถูกต้อง!").not().isEmpty(),
    check('devicetype', "ประเภทอุปกรณ์ไม่ถูกต้อง!").not().isEmpty(),
    check('brand', "ยี่ห้อรุ่นไม่ถูกต้อง!").not().isEmpty(),
    check('model', "จำนวนไม่ถูกต้อง!").not().isEmpty(),
];
exports.save = [
    check('donationperson', "ผู้บริจาคไม่ถูกต้อง!").not().isEmpty(),
    check('devicetype', "ประเภทอุปกรณ์ไม่ถูกต้อง!").not().isEmpty(),
    check('brand', "ยี่ห้อรุ่นไม่ถูกต้อง!").not().isEmpty(),
    check('model', "จำนวนไม่ถูกต้อง!").not().isEmpty(),
];

