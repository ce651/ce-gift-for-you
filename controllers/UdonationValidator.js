const { check } = require('express-validator');

exports.add = [
    check('donationperson', "ชื่อไม่ถูกต้อง!").not().isEmpty(),
    check('phone', "เบอร์ไม่ถูกต้อง!").isInt(),
    check('donationday', "วันที่ไม่ถูกต้อง!").isDate(),
    
];

exports.save = [
    check('donationperson', "ชื่อไม่ถูกต้อง!").not().isEmpty(),
    check('phone', "เบอร์ไม่ถูกต้อง!").isInt(),
    check('donationday', "วันที่ไม่ถูกต้อง!").isDate(),
];