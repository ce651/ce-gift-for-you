const controller ={};
const { validationResult } = require('express-validator');

controller.show = (req, res) => {
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM donatedplace', (err, allDonatedPlaces) => {
            // if (typeof req.session.userid == 'undefined')
            if (err) {
                res.status(500).json(err);
                return;
            }

            // กรองข้อมูลเฉพาะสถานที่บริจาคของผู้ใช้ที่เข้าสู่ระบบ
            const userDonatedPlaces = allDonatedPlaces.filter(place => place.userid === req.session.userid);

            res.render('./donatedplaceview/donatedplaceView', {
                data: userDonatedPlaces,
                session: req.session
            });
        });
    });
};


controller.add = (req, res) => {
    res.render('./donatedplaceview/donatedplaceAdd', {
        session: req.session,
        data: {} // สร้างตัวแปร data เปล่าเพื่อให้ไม่เกิดข้อผิดพลาด data is not defined
    });
};


controller.new = (req, res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success =false;
        return  res.redirect('/donatedplace/add')
    }else{
        req.session.success=true;
        req.session.topic="เพิ่มข้อมูลสำเร็จ!";
        const data = req.body; 
        req.getConnection((err, conn) => {
        conn.query('INSERT INTO donatedplace SET ?', [data], (err, donatedplace) => {
            if (err) {
                res.json(err);
            }
            res.redirect('/donatedplace/list');
        });
    });
}};

controller.delete = (req, res) => {
    const data = req.body.data;
    res.render('./donatedplaceview/donatedplaceDel', {
        data: data,session: req.session
    });
};

controller.delete00 = (req, res) => {
    const idToDelete = req.params.id; // รับค่า id จากพารามิเตอร์ของ URL
    req.session.success = true;
    req.session.topic = "ลบข้อมูลสำเร็จ!";
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM donatedplace WHERE id = ?', [idToDelete], (err, donatedplace) => {
            res.redirect('/donatedplace/list');
        });
    });
};




controller.edit = (req, res) => {
    const idToEdit = req.params.id;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM donatedplace WHERE id = ?', [idToEdit], (err, data) => {
            if (err) {
                return res.status(500).json(err);
            }
            res.render('./donatedplaceview/donatedplaceEdit', { data: data[0],session:req.session });
        });
    });
};

controller.save = (req, res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success =false;
        return  res.redirect('/donatedplace/save/'+ req.params.id)
    }else{
        req.session.success=true;
        req.session.topic="แก้ไขข้อมูลสำเร็จ!";
        const idToEdit = req.params.id;
        const updatedData = {
        place: req.body.place,
        address: req.body.address,
        contactperson: req.body.contactperson,
        phone: req.body.phone,
        

    };
    req.getConnection((err, conn) => {
        conn.query('UPDATE donatedplace SET ? WHERE id = ?', [updatedData, idToEdit], (err, result) => {
            if (err) {
                return res.status(500).json(err);
            }
            res.redirect('/donatedplace/list'); 
        });
    });
}};











module.exports=controller;
