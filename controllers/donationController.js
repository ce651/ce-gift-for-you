const controller ={};
const { validationResult } = require('express-validator');

controller.show=(req,res) => {
    
    req.getConnection((err,conn) =>{
        conn.query('SELECT * FROM donation',(err,approve)=>{
            if(err){
                res.status(500).json(err);
                return;
            }
            res.render('./donationView/donationList',{
                data:approve,session:req.session
            });
        });
    });
}

controller.add = (req, res) => {
    const data = null;
    req.getConnection((err, conn) => {
        res.render('./donationView/donationAdd', { 
            data:data,session: req.session
        });
    });
};

controller.new = (req, res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success =false;
        return  res.redirect('/donation/add')
    }else{
        req.session.success=true;
        req.session.topic="เพิ่มข้อมูลสำเร็จ!";
        const data = req.body; 
        req.getConnection((err, conn) => {
        conn.query('INSERT INTO donation SET ?', [data], (err, donation) => {

            res.redirect('/donation/list');
        });
    });
}};

controller.edit = (req, res) => {
    const {id} = req.params;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM donation WHERE id = ?', [id], (err, data) => {
            if (err) {
                return res.status(500).json(err);
            }
            res.render('./donationView/donationEdit', {
                data:data,
                session:req.session 
            });
        });
    });
}

controller.save = (req, res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success =false;
        return  res.redirect('/donation/edit/'+ req.params.id);
    }else{
        req.session.success=true;
        req.session.topic="แก้ไขข้อมูลสำเร็จ!";
        const {id} = req.params;
        const updatedData = req.body;
    req.getConnection((err, conn) => {
        conn.query('UPDATE donation SET ? WHERE id = ?', [updatedData, id], (err, result) => {
            if (err) {
                return res.status(500).json(err);
            }
            res.redirect('/donation/list'); 
        });
    });
}};


controller.delete = (req, res) => {
    const data = req.body.data;
    res.render('./donationView/confirm', {
        data: data,session: req.session
    });
};

controller.delete1 = (req, res) => {
    const idToDelete = req.params.id; 
    req.session.success = true;
    req.session.topic = "ลบข้อมูลสำเร็จ!";
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM donation WHERE id = ?', [idToDelete], (err, donation) => {
            res.redirect('/donation/list');
        });
    });
};

module.exports=controller;
