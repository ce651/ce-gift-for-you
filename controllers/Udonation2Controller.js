const controller = {};
const { validationResult } = require('express-validator');

controller.show = (req,res) =>{
    req.getConnection((err,conn)=>{
        conn.query('select donation2.id as id,person.name as person,devicetype.name as device, donation.phone,donation.donationday,donationlist.brand as brand,donationlist.model from donation2 join donation on donation2.donation=donation.id join donationlist on donation2.donationlist=donationlist.id  join devicetype on donationlist.devicetype=devicetype.id join person on donation.donationperson=person.id ORDER BY donation2.id  asc',(err,donation2) => {
            if (err){
                res.status(500).json(err);
                return;
            }
            res.render('./Udonation2Views/Udonation2list2',{
                data:donation2,
                session:req.session,
            });
        });
    });
};

controller.add = (req, res) => {
    req.getConnection((err, conn) => {
      if (err) {
        console.error('Error connecting to database:', err);
        res.status(500).json(err);
        return;
      }

      conn.query('SELECT * FROM donation', (err, donation) => {
        if (err) {
          console.error('Error executing SQL query:', err);
          res.status(500).json(err);
          return;
        }

        conn.query('SELECT * FROM donationlist', (err, donationlist) => {
          if (err) {
            console.error('Error executing SQL query:', err);
            res.status(500).json(err);
            return;
          }
          conn.query('SELECT * FROM person', (err, person) => {
            if (err) {
              console.error('Error executing SQL query:', err);
              res.status(500).json(err);
              return;
            }
            conn.query('SELECT * FROM devicetype', (err, devicetype) => {
              if (err) {
                console.error('Error executing SQL query:', err);
                res.status(500).json(err);
                return;
              }
          res.render('./Udonation2Views/Udonation2Add2', {
            data1: donation,
            data2: donationlist,
            data3: {},
            data4: person,
            data5: devicetype,
            session: req.session,
          });
        });
        });
        });
      });
    });
  };
  controller.new = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors.array();
        req.session.success = false;
        return res.redirect('/Udonation2/add');
    } else {
        req.session.success = true;
        req.session.topic = "เพิ่มข้อมูลสำเร็จ!";
        const data = req.body;
    }
    // ดึงข้อมูลที่ถูกส่งมาจากคำขอ HTTP
    const data = req.body;
  
    // ตรวจสอบและแปลงค่าให้ถูกต้อง
    const donation = parseInt(data.donation) || null;
    const donationlist = parseInt(data.donationlist) || null;
    const person = parseInt(data.person) || null;
    const devicetype = parseInt(data.devicetype) || null;
    const values = [donation, donationlist, person, devicetype];

  
      req.getConnection((err, conn) => {
          if (err) {
              res.json(err);
              return;
          }
          conn.query('INSERT INTO donation2(donation, donationlist) VALUES (?, ?)', values, (err, donation2) => {
              if (err) {
                  res.json(err);
                  return;
                }
                conn.query('SELECT * FROM donation', (err, donationData) => {
                  if (err) {
                    console.error('Error executing SQL query:', err);
                    res.status(500).json(err);
                    return;
                  }
                  
                  conn.query('SELECT * FROM donationlist', (err, donationlistData) => {
                    if (err) {
                      console.error('Error executing SQL query:', err);
                      res.status(500).json(err);
                      return;
                    }
                    conn.query('SELECT * FROM devicetype', (err, devicetypeData) => {
                      if (err) {
                        console.error('Error executing SQL query:', err);
                        res.status(500).json(err);
                        return;
                      }
                      conn.query('SELECT * FROM person', (err, personData) => {
                        if (err) {
                          console.error('Error executing SQL query:', err);
                          res.status(500).json(err);
                          return;
                        }
              res.redirect('/Udonation2/list');
            });
          });
        });
      });
    });
  });
};

controller.delete = (req, res) => {
  const data = req.body.data;
    res.render('Udonation2Del2', {
        data: data, session: req.session
    });
};
controller.delete00 = (req, res) => {
  const idToDelete = req.params.id; // รับค่า id จากพารามิเตอร์ของ URL
  req.session.success = true;
  req.session.topic = "ลบข้อมูลสำเร็จ!";
  req.getConnection((err, conn) => {
      conn.query('DELETE FROM donation2 WHERE id = ?', [idToDelete], (err, donation2) => {
          res.redirect('/Udonation2/list');
      });
  });
};

controller.edit = (req, res) => {
  const idToEdit = req.params.id;
  if (!idToEdit) {
    return res.status(400).send('ไม่พบ ID');
  }

  req.getConnection((err, conn) => {
    if (err) {
      console.error('เกิดข้อผิดพลาดในการเชื่อมต่อกับฐานข้อมูล:', err);
      return res.status(500).json(err);
    }

    conn.query('SELECT * FROM donation', (err, donationData) => {
      if (err) {
        console.error('เกิดข้อผิดพลาดในการสั่งคำสั่ง SQL:', err);
        return res.status(500).json(err);
      }

      conn.query('SELECT * FROM donationlist', (err, donationlistData) => {
        if (err) {
          console.error('เกิดข้อผิดพลาดในการสั่งคำสั่ง SQL:', err);
          return res.status(500).json(err);
        }

        conn.query('SELECT * FROM donation2 WHERE id = ?', [idToEdit], (err, donation2Data) => {
          if (err) {
            console.error('เกิดข้อผิดพลาดในการสั่งคำสั่ง SQL:', err);
            return res.status(500).json(err);
          }

          conn.query('SELECT * FROM person', (err, personData) => {
            if (err) {
              console.error('เกิดข้อผิดพลาดในการสั่งคำสั่ง SQL:', err);
              return res.status(500).json(err);
            }

            conn.query('SELECT * FROM devicetype', (err, devicetypeData) => {
              if (err) {
                console.error('เกิดข้อผิดพลาดในการสั่งคำสั่ง SQL:', err);
                return res.status(500).json(err);
              }

              res.render('./Udonation2Views/Udonation2Edit2', {
                data1: donationData,
                data2: donationlistData,
                data3: donation2Data[0], // เลือกข้อมูลแรกจาก donation2Data
                data4: personData,
                data5: devicetypeData,
                session: req.session,
              });
            });
          });
        });
      });
    });
  });
};

controller.save = (req, res) => {
  const errors = validationResult(req);
  const idToEdit = req.params.id;
  
  if (!errors.isEmpty()) {
    req.session.errors = errors.array();
    req.session.success = false;
    return res.redirect('/Udonation2/edit/' + idToEdit);
  } else {
    req.session.success = true;
    req.session.topic = "แก้ไขข้อมูลสำเร็จ!";
    const data = req.body;
    
    req.getConnection((err, conn) => {
      if (err) {
        return res.status(500).json(err);
      } 

      conn.query('UPDATE donation2 SET donation=?, donationlist=? WHERE id = ?', [data.donation, data.donationlist, idToEdit], (err, result) => {
        if (err) {
          return res.status(500).json(err);
        } 
        res.redirect('/Udonation2/list');
      });
    });
  }
};



module.exports = controller;