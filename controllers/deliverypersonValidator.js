const { check } = require('express-validator');

exports.checksave = [
    check('donationplace', 'ข้อมูลไม่ถูกต้อง!').isLength({ min: 1 }),
    check('name', 'ชื่อไม่ถูกต้อง!').isLength({ min: 1 }),
];

exports.checkedit = [
    check('donationplace', 'ข้อมูลไม่ถูกต้อง!').isLength({ min: 1 }),
    check('name', 'ชื่อไม่ถูกต้อง!').isLength({ min: 1 }),
];