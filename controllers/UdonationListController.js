const controller ={};
const { validationResult } = require('express-validator');


controller.show=(req,res) => {
    req.getConnection((err,conn) =>{
        conn.query('SELECT d.id as id,p.name as name,de.name as dename,d.brand as brand,d.model as model from  donationlist as d join person as p on d.donationperson = p.id join devicetype as de on d.devicetype = de.id ',(err,donation)=>{
            console.log("hello");
            res.render('./UdonationlistView/UdonationListList',{
                data:donation,session:req.session
            });
        });
    });
};

controller.add = (req, res) => {
    req.getConnection((err, conn) => 
        conn.query('SELECT * FROM person',(err, cargo) => { 
            conn.query('SELECT * FROM devicetype',(err, carto) => { 
                res.render('./UdonationlistView/UdonationListAdd', {
                    data1:cargo,data2:carto,session: req.session,data:{} 
                    })
                })
            }) 
        )};

        controller.add = (req, res) => {
            req.getConnection((err, conn) => 
                conn.query('SELECT * FROM person',(err, cargo) => { 
                    conn.query('SELECT * FROM devicetype',(err, carto) => { 
                        res.render('./UdonationlistView/UdonationListAdd', {
                            data1:cargo,data2:carto,session: req.session,data:{} 
                            })
                        })
                    }) 
                )};

controller.new = (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        return res.redirect('/UdonationList/add');
    } else {
        req.session.success = true;
        req.session.topic = "เพิ่มข้อมูลสำเร็จ!";
        const data = req.body;
        req.getConnection((err, conn) => {
            conn.query('INSERT INTO donationlist SET ?', [data], (err, donation) => {
                if (err) {
                    res.json(err);
                }
                res.redirect('/UdonationList/list');
            });
        });
    }
};

controller.delete = (req, res) => {
    const data = req.body.data;
    res.render('./UdonationlistView/confirdonation', {
        data: data,session: req.session
    });
};

controller.delete = (req, res) => {
    const {id} = req.params;
    req.session.success = true;
    req.session.topic = "ลบข้อมูลสำเร็จ!";
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM donationlist WHERE id = ?', [id], (err, donation) => {
            res.redirect('/UdonationList/list');
        });
    });
};

controller.edit = (req, res) => {
    const idToEdit = req.params.id;
    req.getConnection((err, conn) => {
        conn.query('SELECT * FROM person',(err, cargo) => { 
            conn.query('SELECT * FROM devicetype',(err, carto) => { 
                conn.query('SELECT * FROM donationlist WHERE id = ?', [idToEdit], (err, data) => {
                    res.render('./UdonationlistView/UdonationListEdit', { 
                        data1:cargo,data2:carto,data3:data,session:req.session 
                    });
                })
            })
        })              
    })
};

controller.save = (req, res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success =false;
        return  res.redirect('/UdonationList/edit/'+ req.params.id)
    }else{
        req.session.success=true;
        req.session.topic="แก้ไขข้อมูลสำเร็จ!";
        const {id} = req.params;
        const updatedData = {
        donationperson: req.body.donationperson,
        devicetype: req.body.devicetype,
        brand: req.body.brand,
        model: req.body.model,
    };

    req.getConnection((err, conn) => {
        conn.query('UPDATE donationlist SET ? WHERE id = ?', [updatedData, id], (err, result) => {
            if (err) {
                return res.status(500).json(err);
            }
            res.redirect('/UdonationList/list'); 
        });
    });
}};

module.exports=controller;
