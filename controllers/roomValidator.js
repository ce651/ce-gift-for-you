const { check } = require('express-validator');

exports.room = [
    check('roomname', "ชื่อไม่ถูกต้อง!").not().isEmpty(),
];