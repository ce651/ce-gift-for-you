const express = require('express');
const router = express.Router();
const Udonation2Controller = require('../controllers/Udonation2Controller');
const validator = require('../controllers/Udonation2Validator');

router.get('/Udonation2/list',Udonation2Controller.show);

router.get('/Udonation2/add',Udonation2Controller.add);
router.post('/Udonation2/add', validator.Udonation2, Udonation2Controller.new);

router.get('/Udonation2/delete/:id', Udonation2Controller.delete);
router.post('/Udonation2/delete/:id',Udonation2Controller.delete00);

router.get('/Udonation2/edit/:id', Udonation2Controller.edit);
router.post('/Udonation2/edit/:id',validator.save, Udonation2Controller.save);


module.exports = router;