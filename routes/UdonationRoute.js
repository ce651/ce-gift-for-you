const express=require('express');
const router=express.Router();
const UdonationController = require('../controllers/UdonationController');
const Udonationvalidator = require('../controllers/UdonationValidator');

router.get('/Udonation/list', UdonationController.show);

router.get('/Udonation/add', UdonationController.add);
router.post('/Udonation/add', Udonationvalidator.add, UdonationController.new);

router.get('/Udonation/edit/:id', UdonationController.edit);
router.post('/Udonation/edit/:id', Udonationvalidator.save, UdonationController.save);

router.get('/Udonation/delete/:id', UdonationController.delete);
router.post('/Udonation/delete/:id', UdonationController.delete1);

module.exports = router;








