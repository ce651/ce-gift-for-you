const express=require('express');
const router=express.Router();
const roomController=require('../controllers/roomController');
const Validator = require('../controllers/roomValidator');

router.get('/room/list', roomController.show);

router.get('/room/add', roomController.add);
router.post('/room/add', Validator.room, roomController.new);

router.get('/room/edit/:id', roomController.edit);
router.post('/room/edit/:id', Validator.room, roomController.save);

router.get('/room/delete/:id', roomController.delete);
router.post('/room/delete/:id', roomController.delete1);

module.exports = router;