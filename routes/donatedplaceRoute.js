const express=require('express');
const router=express.Router();
const donatedplaceController=require('../controllers/donatedplaceController');
const validator = require('../controllers/donatedplaceValidator');



router.get('/donatedplace/list',donatedplaceController.show);

router.get('/donatedplace/add', donatedplaceController.add);
router.post('/donatedplace/add', validator.donatedplace, donatedplaceController.new);

router.get('/donatedplace/edit/:id', donatedplaceController.edit);

router.post('/donatedplace/edit/:id', validator.save, donatedplaceController.save);


router.get('/donatedplace/delete/:id', donatedplaceController.delete);
router.post('/donatedplace/delete/:id',donatedplaceController.delete00);





module.exports = router;