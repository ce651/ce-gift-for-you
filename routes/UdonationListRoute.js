const express=require('express');
const router=express.Router();
const UdonationListController=require('../controllers/UdonationListController');
const validator = require('../controllers/UdonationListValidator');


router.get('/UdonationList/list', UdonationListController.show);

router.get('/UdonationList/add', UdonationListController.add);
router.post('/UdonationList/add', validator.UdonationList, UdonationListController.new);

router.get('/UdonationList/edit/:id', UdonationListController.edit);
router.post('/UdonationList/edit/:id', validator.save, UdonationListController.save);


router.get('/UdonationList/delete/:id', UdonationListController.delete);
router.post('/UdonationList/delete/:id', UdonationListController.delete);


module.exports = router;