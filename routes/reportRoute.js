const express=require('express');
const router=express.Router();
const reportController=require('../controllers/reportController');
const validator = require('../controllers/reportValidator');

router.get('/report/list',reportController.show);

router.get('/report/add', reportController.add);
router.post('/report/add', validator.cra, reportController.new);

router.get('/report/edit/:id', reportController.edit);
router.post('/report/edit/:id', validator.crs, reportController.save);


router.get('/report/delete/:id', reportController.delete);
router.post('/report/delete/:id',reportController.delete00);

module.exports = router;