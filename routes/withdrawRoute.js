const express = require('express');
const router = express.Router();
const withdrawController = require('../Controller/withdrawController');
const validator = require('../Controller/withdrawvalidator');

router.get('/withdraw/list', withdrawController.show);

router.get('/withdraw/add', withdrawController.add)
router.post('/withdraw/add',validator.checkwithdraw,withdrawController.new);

router.get('/withdraw/edit/:id',withdrawController.edit);
router.post('/withdraw/edit/:id',validator.save,withdrawController.save);

router.get('/withdraw/delete/:id',withdrawController.delete);
router.post('/withdraw/delete/:id',withdrawController.delete1);

module.exports = router;