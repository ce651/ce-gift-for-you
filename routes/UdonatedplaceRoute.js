const express=require('express');
const router=express.Router();
const UdonatedplaceController=require('../controllers/UdonatedplaceController');
const validator = require('../controllers/UdonatedplaceValidator');
// const isAuthenticated = require('../controllers/isAuthenticated');



router.get('/Udonatedplace/list',UdonatedplaceController.show);

router.get('/Udonatedplace/add', UdonatedplaceController.add);
router.post('/Udonatedplace/add', validator.Udonatedplace, UdonatedplaceController.new);

router.get('/Udonatedplace/edit/:id', UdonatedplaceController.edit);

router.post('/Udonatedplace/edit/:id', validator.save, UdonatedplaceController.save);


router.get('/Udonatedplace/delete/:id', UdonatedplaceController.delete);
router.post('/Udonatedplace/delete/:id',UdonatedplaceController.delete00);



// router.get('/Udonatedplace/list', isAuthenticated, UdonatedplaceController.list);
// router.get('/Udonatedplace/show', isAuthenticated, UdonatedplaceController.show);






module.exports = router;