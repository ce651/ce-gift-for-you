// นำเข้า Express.js และ express-validator
const express = require('express');
const router = express.Router();

// นำเข้า Controller และ Validator
const prepareController = require('../controllers/prepareController');
const prepareValidator = require('../controllers/prepareValidator');

// เส้นทางแสดงรายการสถานที่และวันที่บริจาค
router.get('/prepare/list', prepareController.show);

// เส้นทางเพิ่มรายการสถานที่และวันที่บริจาค
router.get('/prepare/add', prepareController.add);
router.post('/prepare/add', prepareValidator.add,prepareController.new);
router.get('/prepare/edit/:id', prepareController.edit);
router.post('/prepare/edit/:id', prepareValidator.save, prepareController.save);
router.get('/prepare/delete/:id', prepareController.delete);
router.post('/prepare/delete/:id', prepareController.delete1);


module.exports = router;
