const express=require('express');
const router=express.Router();
const dutyController=require('../controllers/dutyController');
const validator = require('../controllers/validator');

router.get('/duty/list', dutyController.show);

router.get('/duty/add', dutyController.add)
router.post('/duty/add',validator.checkdu, dutyController.new);

router.get('/dutyEdit/:id',dutyController.edit);
router.post('/dutyEdit/:id',validator.duty,dutyController.save);

router.get('/deleteduty/:id',dutyController.delete);
router.post('/deleteduty/:id',dutyController.delete1);

module.exports = router;
