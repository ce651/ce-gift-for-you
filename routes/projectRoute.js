const express = require('express');
const router = express.Router();
const projectController = require('../controllers/projectController');
const projectValidator = require('../controllers/projectValidator');


router.get('/project/list', projectController.show);

router.get('/project/add', projectController.add);
router.post('/project/add', projectValidator.add, projectController.new);

router.get('/project/edit/:id', projectController.edit);
router.post('/project/edit/:id', projectValidator.save, projectController.save);


router.get('/project/delete/:id', projectController.delete);
router.post('/project/delete/:id', projectController.delete1);

module.exports = router;
