const express=require('express');
const router=express.Router();
const donationController = require('../controllers/donationController');
const donationvalidator = require('../controllers/donationValidator');

router.get('/donation/list', donationController.show);

router.get('/donation/add', donationController.add);
router.post('/donation/add', donationvalidator.add, donationController.new);

router.get('/donation/edit/:id', donationController.edit);
router.post('/donation/edit/:id', donationvalidator.save, donationController.save);

router.get('/donation/delete/:id', donationController.delete);
router.post('/donation/delete/:id', donationController.delete1);

module.exports = router;








