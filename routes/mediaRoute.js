const express=require('express');
const router=express.Router();
const mediaController=require('../controllers/mediaController');
const validator = require('../controllers/validator');

router.get('/media/list', mediaController.show);

router.get('/media/add', mediaController.add)
router.post('/media/add',validator.checkme, mediaController.new);

router.get('/mediaEdit/:id',mediaController.edit);
router.post('/mediaEdit/:id',validator.media,mediaController.save);

router.get('/deletemedia/:id',mediaController.delete);
router.post('/deletemedia/:id',mediaController.delete1);

module.exports = router;
