const express=require('express');
const body=require('body-parser'); 
const cookie=require('cookie-parser');
const session=require('express-session');
const mysql=require('mysql');
const connection=require('express-myconnection');
const app=express();
const path = require('path');
const listofapprovedpeople = require('./routes/listofapprovedpeopleRoute');
const donatedplaceRoutes = require('./routes/donatedplaceRoute');
const reportRoutes = require('./routes/reportRoute');

app.set('view engine','ejs');
app.use(express.static('public'));
app.use(body.urlencoded({extended: true})); 
app.use(cookie());
app.use(session({
    secret:'12',
    resave:true,
    saveUninitialized: true
}));

app.use(connection(mysql,{
    host:'localhost',
    user:'admin',
    password:'admin1234567',
    port:'3306',
    database:'ce_gift_for_you'
},'single'));   

app.use('/', donatedplaceRoutes); 
app.use('/', listofapprovedpeople);
app.use('/', reportRoutes);




app.listen('8089');